-- Add Users
create sequence _users_id_seq;

alter sequence _users_id_seq owner to postgres;

create table _users
(
    id bigint default nextval('_users_id_seq'::regclass) not null
        constraint users_pk
            primary key,
    roleAdmin boolean default false not null,
    username varchar not null unique,
    email varchar(100) unique,
    enabled boolean default true not null,
    password varchar(100) not null,
    created_by varchar(50),
    created_date timestamp,
    last_modified_by varchar(50),
    last_modified_date timestamp
);

alter table _users
    owner to postgres;

create unique index users_id_uindex on _users (id);

alter sequence _users_id_seq owned by _users.id;


--Add Category
create sequence _tab_id_seq;

alter sequence _tab_id_seq owner to postgres;

create table _tab
(
    id bigint default nextval('_tab_id_seq'::regclass) not null
        constraint _tab_pk
        primary key,
    name varchar not null,
    created_by varchar(50),
    created_date timestamp,
    last_modified_by varchar(50),
    last_modified_date timestamp
);

alter table _tab
    owner to postgres;

create unique index _tab_id_uindex
    on _tab (id);




-- Add News
create sequence _news_id_seq;

alter sequence _news_id_seq owner to postgres;

create table _news
(
    id bigint
        default nextval('_news_id_seq'::regclass) not null
        constraint _news_pk
            primary key,


    title             text,
    description       text,
    short_description text,
    content           text,
    published_date    timestamp,
    author_id bigint
        constraint _news_users_id_fk references _users,
    tab_id bigint
        constraint _news_tab_id_fk references _tab,
    status boolean,
    created_by varchar(50),
    created_date timestamp,
    last_modified_by varchar(50),
    last_modified_date timestamp
);

alter table _news
    owner to postgres;

create unique index _news_id_uindex
    on _news (id);

alter sequence _news_id_seq owned by _news.id;

-- Add Comment
create sequence _news_comment_id_seq;

alter sequence _news_comment_id_seq owner to postgres;

create table _news_comment
(
    id bigint default nextval('_news_comment_id_seq'::regclass) not null
        constraint _news_comment_pk
        primary key,
    text varchar not null,
    user_id bigint not null
        constraint _news_comment_users_id_fk
        references _users,
    news_id bigint not null
        constraint _news_comment_news_id_fk
        references _news,
    created_by varchar(50),
    created_date timestamp,
    last_modified_by varchar(50),
    last_modified_date timestamp
);

alter table _news_comment
    owner to postgres;

create unique index _news_comment_id_uindex
    on _news_comment (id);


-- Add Like
create sequence _news_like_id_seq;

alter sequence _news_like_id_seq owner to postgres;

create table _news_like
(
    id bigint default nextval('_news_like_id_seq'::regclass) not null
        constraint _news_like_pk
        primary key,
    status boolean default false not null,
    user_id bigint not null
        constraint _news_like_users_id_fk
        references _users,
    news_id bigint not null
        constraint _news_like_news_id_fk
        references _news,
    created_by varchar(50),
    created_date timestamp,
    last_modified_by varchar(50),
    last_modified_date timestamp
);

alter table _news_like
    owner to postgres;

create unique index _news_like_id_uindex
    on _news_like (id);

