package kz.news.it.service;

import kz.news.it.entity.Category;
import kz.news.it.entity.PageableCustom;

import java.util.List;
import java.util.Map;

public interface ICategoryService {
    Category createCategory(Category body, String user);
    List<Category> getAllCategories();
    Category getCategory(Long categoryId);
    Category updateCategory(Long categoryId, Category body, String user);
    void deleteCategory(Long categoryId, String user);
    PageableCustom getFilteredCategories(Map<String, String> params);
}
