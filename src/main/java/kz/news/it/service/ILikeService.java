package kz.news.it.service;

import kz.news.it.entity.Like;

public interface ILikeService {
    Like createNewsLike(Like body);
    void deleteAllByNewsId(Long newsId);
    void deleteAllNewsLikeByTabId(Long tabId);
    Long getLikeCountByNewsId(Long newsId);
    Boolean isLiked(Long newsId, String username);
}
