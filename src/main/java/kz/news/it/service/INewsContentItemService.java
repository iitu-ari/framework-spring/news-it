package kz.news.it.service;


import kz.news.it.model.NewsContentItem;

import java.util.List;

public interface INewsContentItemService {
    NewsContentItem getNewsContentById(String id);
    NewsContentItem getNewsContentByNewsId(Long newsId);
    NewsContentItem createNewsContent(NewsContentItem newsContentItem);
    List<NewsContentItem> globalSearch(String value);
}
