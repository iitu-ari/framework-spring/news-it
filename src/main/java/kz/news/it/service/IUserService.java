package kz.news.it.service;

import kz.news.it.entity.PageableCustom;
import kz.news.it.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

public interface IUserService {
    User saveUser(User userEntity);
    Page<User> getAllUsers(Integer pageNo, Integer pageSize, String sortBy);
    User getUserById(Long id);
    List<User> getUsersById(List<Long> idList);
    PageableCustom getAllUserWithParams(@RequestParam Map<String, String> params);
    void deleteUserById(Long id);
    boolean blockUser(Long id);
    Long getUserIdByUsername(String username);
    User unlockUserById(Long id);
}
