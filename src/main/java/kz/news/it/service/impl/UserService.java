package kz.news.it.service.impl;

import kz.news.it.entity.PageableCustom;
import kz.news.it.entity.User;
import kz.news.it.repository.UserRepository;
import kz.news.it.service.IUserService;
import lombok.AllArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class UserService implements IUserService {
    private final UserRepository userRepository;

    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    PasswordEncoder encoder;

    public User saveUser(User userEntity) {
        if (userEntity.getId() != null) {
            User userEntityToUpd = userRepository.getById(userEntity.getId());
            userEntityToUpd.setUsername(userEntity.getUsername());
            return userRepository.save(userEntityToUpd);
        }
        return userRepository.save(userEntity);
    }


    @Override
    public Page<User> getAllUsers(Integer pageNo, Integer pageSize, String sortBy) {
        Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).ascending());
        Page<User> users = userRepository.findAll(pageable);
        return users;
    }

    @Override
    public User getUserById(Long id) {
        User userEntity = userRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("users", "userId: " + id));

        return userEntity;

    }

    @Override
    public List<User> getUsersById(List<Long> idList) {
        return userRepository.findByIdIn(idList);
    }

    @Override
    public PageableCustom getAllUserWithParams(@RequestParam Map<String, String> params) {
        int pageNumber = 0;
        int pageSize = 10;
        String sortBy = "id";

        if (params.containsKey("pageNumber")) {
            pageNumber = Integer.parseInt(params.get("pageNumber"));
        }
        if (params.containsKey("pageSize")) {
            pageSize = Integer.parseInt(params.get("pageSize"));
        }
        if (params.containsKey("sortBy")) {
            sortBy = params.get("sortBy");
        }
        Sort sort = Sort.by(sortBy).ascending();
        if (params.containsKey("direction") && params.get("direction").equals("desc")) {
            sort = Sort.by(sortBy).descending();
        }

        final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize, sort);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> cq = cb.createQuery(User.class);
        Root<User> iRoot = cq.from(User.class);
        List<Predicate> predicates = new ArrayList<>();


        if (params.containsKey("searchString")) {
            predicates.add(cb.or(cb.like(cb.lower(iRoot.get("username")), "%" + params.get("searchString").toLowerCase() + "%")));
        }
//        predicates.add(cb.and(iRoot.get("deletedDate").isNull()));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);

        cq.where(predArray);

        TypedQuery<User> query = entityManager.createQuery(cq);
        int totalRows = query.getResultList().size();

        query.setFirstResult(pageableRequest.getPageNumber() * pageableRequest.getPageSize());
        query.setMaxResults(pageableRequest.getPageSize());

        Page<User> result = new PageImpl<>(query.getResultList(), pageableRequest, totalRows);

        PageableCustom pageableCustom = new PageableCustom();

        pageableCustom.setContent(result.getContent());
        pageableCustom.setPage(result.getNumber());
        pageableCustom.setSize(result.getSize());
        pageableCustom.setTotal(result.getTotalElements());

        return pageableCustom;
    }

    @Override
    public void deleteUserById(Long id) {
        userRepository.findById(id).ifPresent(user -> {
            userRepository.deleteById(id);
        });


    }

    @Override
    public boolean blockUser(Long id) {
        User userEntity = userRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("users", "userId: " + id));
        userEntity.setEnabled(false);
        userRepository.saveAndFlush(userEntity);
        return true;
    }

    @Override
    public Long getUserIdByUsername(String username) {
        User user = userRepository.findByUsername(username);

        return user.getId();
    }

    @Override
    public User unlockUserById(Long id) {
        User userEntity = userRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("users", "userId: " + id));

        userEntity.setEnabled(true);
        userRepository.saveAndFlush(userEntity);
        return userEntity;
    }

//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        User user = userRepository.findByUsername(username).get();
//        if (user != nu)
//
//    }

}
