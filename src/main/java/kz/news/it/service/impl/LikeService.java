package kz.news.it.service.impl;

import kz.news.it.entity.Like;
import kz.news.it.repository.LikeRepository;
import kz.news.it.service.ILikeService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@AllArgsConstructor
public class LikeService implements ILikeService {

    private static final Logger LOG = LoggerFactory.getLogger(NewsService.class);

    private final LikeRepository newsLikeRepository;

    private final UserService userService;

    @Override
    public Like createNewsLike(Like body) {
        Like newsLikeEntityinDb = newsLikeRepository.findByUserIdAndNewsId(body.getUserId(), body.getNewsId());
        if (newsLikeEntityinDb == null) {
            return newsLikeRepository.save(body);
        } else {
            if (newsLikeEntityinDb.getStatus().equals(body.getStatus())) {
                newsLikeRepository.deleteById(newsLikeEntityinDb.getId());
                return null;
            } else {
                newsLikeEntityinDb.setStatus(body.getStatus());
                return newsLikeRepository.save(newsLikeEntityinDb);
            }
        }
    }

    @Transactional
    @Override
    public void deleteAllByNewsId(Long newsId) {
        LOG.debug("deleteAllByNewsId: tries to delete all entities with newsId: {}", newsId);
        newsLikeRepository.deleteAllByNewsId(newsId);
    }

    @Override
    public void deleteAllNewsLikeByTabId(Long tabId) {
        LOG.debug("deleteAllNewsLikeBTabId: tries to delete all entities with tabId: {}", tabId);
        newsLikeRepository.deleteAllByTabId(tabId);
    }

    @Override
    public Long getLikeCountByNewsId(Long newsId){
        return newsLikeRepository.countAllByNewsId(newsId);
    }

    @Override
    public Boolean isLiked(Long newsId, String username){
        Long userId = userService.getUserIdByUsername(username);
        Like entity =  newsLikeRepository.findByUserIdAndNewsId(userId, newsId);
        return entity != null && entity.getStatus();
    }
}
