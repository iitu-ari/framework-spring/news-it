package kz.news.it.service.impl;

import kz.news.it.entity.Category;
import kz.news.it.entity.PageableCustom;
import kz.news.it.repository.CategoryRepository;
import kz.news.it.repository.CommentRepository;
import kz.news.it.repository.LikeRepository;
import kz.news.it.repository.NewsRepository;
import kz.news.it.service.ICategoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
@Slf4j
public class CategoryService implements ICategoryService {

    private static final Logger LOG = LoggerFactory.getLogger(NewsService.class);

    private final CategoryRepository categoryRepository;

    private final NewsRepository newsRepository;
    private final CommentRepository newsCommentRepository;
    private final LikeRepository likeRepository;

    @Autowired
    KafkaTemplate<String, Category> kafkaTemplate;
    private static final String TOPIC = "mine";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Category createCategory(Category body, String user) {
        body.setCreatedBy(user);
        body.setLastModifiedBy(user);
        return categoryRepository.save(body);
    }

    @Override
    public List<Category> getAllCategories() {
        return categoryRepository.findAll();
    }

    @Override
    public Category getCategory(Long categoryId) {
        Category categoryEntity = categoryRepository.findById(categoryId)
                .orElseThrow();
        LOG.debug("getTab: found tabId: {}", categoryEntity.getId());
        return categoryEntity;
    }

    @Override
    public Category updateCategory(Long categoryId, Category body, String user) {
        Category entity = categoryRepository.findById(categoryId).orElseThrow();

        entity.setName(body.getName());
        entity.setLastModifiedBy(user);
        entity.setLastModifiedDate(LocalDateTime.now());
        return categoryRepository.save(entity);
    }


    @Transactional
    @Override
    public void deleteCategory(Long categoryId, String user) {

        LOG.info("deleteTab: tries to delete an entity with tabId: {}", categoryId);
        categoryRepository.findById(categoryId).ifPresent(o -> {
            newsCommentRepository.deleteAllByTabId(categoryId);
            likeRepository.deleteAllByTabId(categoryId);
            log.info(post(categoryId, user));
            categoryRepository.deleteById(categoryId);
        });
    }

    public String post(Long categoryId, String user) {
        kafkaTemplate.send(TOPIC, new Category(categoryId, "Aray"));

        return "Aray deleted category with id " + categoryId + ". Published to kafka successfully";
    }

    @Override
    public PageableCustom getFilteredCategories(Map<String, String> params) {
        int pageNumber = 0;
        int pageSize = 10;

        if (params.containsKey("pageNumber")) {
            pageNumber = Integer.parseInt(params.get("pageNumber"));
        }
        if (params.containsKey("pageSize")) {
            pageSize = Integer.parseInt(params.get("pageSize"));
        }

        final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Category> cq = cb.createQuery(Category.class);
        Root<Category> iRoot = cq.from(Category.class);
        List<Predicate> predicates = new ArrayList<>();

        if (params.containsKey("searchString")) {
            predicates.add(cb.or(cb.like(cb.lower(iRoot.get("name")), "%" + params.get("searchString").toLowerCase() + "%")));
        }

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);

        cq.where(predArray);

        TypedQuery<Category> query = entityManager.createQuery(cq);
        int totalRows = query.getResultList().size();

        query.setFirstResult(pageableRequest.getPageNumber() * pageableRequest.getPageSize());
        query.setMaxResults(pageableRequest.getPageSize());

        Page<Category> result = new PageImpl<>(query.getResultList(), pageableRequest, totalRows);

        PageableCustom pageableCustom = new PageableCustom();

        pageableCustom.setContent(query.getResultList());
        pageableCustom.setPage(result.getNumber());
        pageableCustom.setSize(result.getSize());
        pageableCustom.setTotal(result.getTotalElements());

        return pageableCustom;
    }

}
