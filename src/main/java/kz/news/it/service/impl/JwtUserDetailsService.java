package kz.news.it.service.impl;

import kz.news.it.entity.User;
import kz.news.it.entity.UserDTO;
import kz.news.it.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(),
                new ArrayList<>());
    }

    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public User save(UserDTO userDTO) {
        User userRepo = userRepository.findByUsername(userDTO.getUsername());
        if (userRepo == null) {
            User user = new User();
            user.setUsername(userDTO.getUsername());
            user.setEmail(userDTO.getEmail());
            if (userDTO.getIsAdmin() != null) {
                user.setRoleAdmin(userDTO.getIsAdmin());
            } else user.setRoleAdmin(false);
            user.setPassword(bcryptEncoder.encode(userDTO.getPassword()));
            return userRepository.saveAndFlush(user);
        } else {
            return userRepo;
        }
    }

}
