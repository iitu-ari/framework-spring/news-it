package kz.news.it.service.impl;

import kz.news.it.model.NewsContentItem;
import kz.news.it.repository.NewsContentItemsRepository;
import kz.news.it.service.INewsContentItemService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
public class NewsContentItemService implements INewsContentItemService {

    private final NewsContentItemsRepository newsContentItemsRepository;

    @Override
    public NewsContentItem getNewsContentById(String id) {
        System.out.println("get news from mongo: " + id);
        return newsContentItemsRepository.findById(id).get();
    }

    @Override
    public NewsContentItem getNewsContentByNewsId(Long newsId) {
        System.out.println("get news from mongo by newsId: " + newsId);
        return newsContentItemsRepository.findByNewsId(newsId);
    }

    @Override
    public NewsContentItem createNewsContent(NewsContentItem newsContentItem) {
        return newsContentItemsRepository.save(newsContentItem);
    }

    @Override
    public List<NewsContentItem> globalSearch(String value) {
        return newsContentItemsRepository.globalSearch(value);
    }
}
