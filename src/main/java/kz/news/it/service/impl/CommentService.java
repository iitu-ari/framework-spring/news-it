package kz.news.it.service.impl;

import kz.news.it.entity.Comment;
import kz.news.it.entity.PageableCustom;
import kz.news.it.repository.CommentRepository;
import kz.news.it.service.ICommentService;
import org.slf4j.Logger;
import lombok.AllArgsConstructor;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
public class CommentService implements ICommentService {

    private static final Logger LOG = LoggerFactory.getLogger(NewsService.class);

    private final CommentRepository newsCommentRepository;
    private final UserService userService;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Comment createNewsComment(Comment body, String user) {
        body.setCreatedBy(userService.getUserById(body.getUserId()).getUsername());
        body.setLastModifiedBy(userService.getUserById(body.getUserId()).getUsername());
        return newsCommentRepository.saveAndFlush(body);
    }

    @Override
    public Comment getNewsComment(Long newsCommentId) {
        Comment response = newsCommentRepository.findById(newsCommentId)
                .orElseThrow();
        LOG.debug("getNewsComment: found newsCommentId: {}", response.getId());
        return response;
    }

    @Override
    public List<Comment> getAllNewsCommentByNewsId(Long newsId) {

        List<Comment> newsCommentList = newsCommentRepository.findAllByNewsId(newsId);
        return newsCommentList.stream()
                .map( newsComment -> {
                    Comment comment = new Comment();
                    comment.setId(newsComment.getId());
                    comment.setText(newsComment.getText());
                    comment.setUserId(newsComment.getUserId());
                    comment.setNewsId(newsComment.getNewsId());
                    comment.setCreatedDate(newsComment.getCreatedDate());
                    comment.setCreatedBy(newsComment.getCreatedBy());
                    return comment;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<Comment> getAllNewsCommentByUserId(Long authorId) {

        List<Comment> newsCommentList = newsCommentRepository.findAllByUserId(authorId);
        return newsCommentList.stream()
                .map( newsComment -> {
                    Comment comment = new Comment();
                    comment.setId(newsComment.getId());
                    comment.setText(newsComment.getText());
                    comment.setUserId(newsComment.getUserId());
                    comment.setNewsId(newsComment.getNewsId());
                    comment.setCreatedDate(newsComment.getCreatedDate());
                    comment.setCreatedBy(newsComment.getCreatedBy());
                    return comment;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<Comment> getAllNewsCommentByUserIdAndNewsId(Long authorId, Long newsId) {

        List<Comment> newsCommentList = newsCommentRepository.findAllByUserIdAndNewsId(authorId, newsId);
        return newsCommentList.stream()
                .map( newsComment -> {
                    Comment comment = new Comment();
                    comment.setId(newsComment.getId());
                    comment.setText(newsComment.getText());
                    comment.setUserId(newsComment.getUserId());
                    comment.setNewsId(newsComment.getNewsId());
                    comment.setCreatedDate(newsComment.getCreatedDate());
                    comment.setCreatedBy(newsComment.getCreatedBy());
                    return comment;
                })
                .collect(Collectors.toList());
    }

    @Override
    public PageableCustom getAllNewsCommentByNewsIdPagination(Map<String, String> params) {
        //return newsRepository.findAllByDeletedDateIsNull().stream().map(newsMapper::entityToApi).collect(Collectors.toList());
        int pageNumber = 0;
        int pageSize = 10;

        if (params.containsKey("pageNumber")) {
            pageNumber = Integer.parseInt(params.get("pageNumber"));
        }
        if (params.containsKey("pageSize")) {
            pageSize = Integer.parseInt(params.get("pageSize"));
        }


        final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Comment> cq = cb.createQuery(Comment.class);
        Root<Comment> iRoot = cq.from(Comment.class);
        List<Predicate> predicates = new ArrayList<>();
        //status


        if (params.containsKey("newsId")) {
            predicates.add(cb.equal(iRoot.get("newsId"), params.get("newsId")));   //////
        }

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);

        cq.where(predArray);

        TypedQuery<Comment> query = entityManager.createQuery(cq);
        int totalRows = query.getResultList().size();

        query.setFirstResult(pageableRequest.getPageNumber() * pageableRequest.getPageSize());
        query.setMaxResults(pageableRequest.getPageSize());

        Page<Comment> result = new PageImpl<>(query.getResultList(), pageableRequest, totalRows);

        List<Comment> tabs = query.getResultList().stream()
                .map( newsComment -> {
                    Comment comment = new Comment();
                    comment.setId(newsComment.getId());
                    comment.setText(newsComment.getText());
                    comment.setUserId(newsComment.getUserId());
                    comment.setNewsId(newsComment.getNewsId());
                    comment.setCreatedDate(newsComment.getCreatedDate());
                    comment.setCreatedBy(newsComment.getCreatedBy());
                    //userList.stream().filter(user -> Objects.equals(user.getId(), newsComment.getUserId())).findFirst().ifPresent(comment::setUser);
                    return comment;
                })
                .toList();

        PageableCustom pageableCustom = new PageableCustom();

        pageableCustom.setContent(tabs);
        pageableCustom.setPage(result.getNumber());
        pageableCustom.setSize(result.getSize());
        pageableCustom.setTotal(result.getTotalElements());

        return pageableCustom;
    }

    @Override
    public Comment updateNewsComment(Long newsCommentId, Comment body, String user) {
        Comment entity = newsCommentRepository.findById(newsCommentId)
                .orElseThrow();
        entity.setText(body.getText());
        entity.setUserId(body.getUserId());
        entity.setNewsId(body.getNewsId());
        entity.setLastModifiedBy(user);
        entity.setLastModifiedDate(LocalDateTime.now());
        entity.setCreatedBy(body.getCreatedBy());

        Comment newEntity = newsCommentRepository.save(entity);

        LOG.debug("updateNewsComment: updated an entity with newsCommentId: {}", newsCommentId);

        return newEntity;
    }

    @Override
    public void deleteNewsComment(Long newsCommentId, String user) {
        LOG.debug("deleteNewsComment: tries to delete an entity with newsCommentId: {}", newsCommentId);
        newsCommentRepository.findById(newsCommentId).ifPresent(o -> {
            newsCommentRepository.deleteById(newsCommentId);
        });
    }

    @Override
    public void deleteAllNewsCommentByNewsId(Long newsId, String user) {
        LOG.debug("deleteAllNewsCommentByNewsId: tries to delete all entities with newsId: {}", newsId);
        List<Comment> newsCommentEntityList = newsCommentRepository.findAllByNewsId(newsId).stream()
                .peek(o -> {
                    newsCommentRepository.deleteById(o.getId());
                }).toList();
    }

    @Override
    @Transactional
    public void deleteAllNewsCommentByTabId(Long tabId, String user) {
        LOG.debug("deleteAllNewsCommentByTabId: tries to delete all entities with tabId: {}", tabId);
        newsCommentRepository.deleteAllByTabId(tabId);
    }

    @Override
    @Transactional
    public void deleteAllNewsCommentByAuthorId(Long authorId){
        newsCommentRepository.deleteAllByUserId(authorId);
    }

    @Override
    @Transactional
    public void deleteAllNewsCommentByAuthorIdAndNewsId(Long authorId, Long newsId){
        newsCommentRepository.deleteAllByUserIdAndNewsId(authorId, newsId);
    }

    @Override
    public Long getCommentCountByNewsId(Long newsId){
        return newsCommentRepository.countAllByNewsId(newsId);
    }
}

