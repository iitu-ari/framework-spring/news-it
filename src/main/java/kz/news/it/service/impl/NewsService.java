package kz.news.it.service.impl;

import kz.news.it.entity.Category;
import kz.news.it.entity.News;
import kz.news.it.entity.PageableCustom;
import kz.news.it.entity.User;
import kz.news.it.model.NewsContentItem;
import kz.news.it.repository.NewsRepository;
import kz.news.it.service.INewsContentItemService;
import kz.news.it.service.INewsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


@Service
@Slf4j
@AllArgsConstructor
public class NewsService implements INewsService {
    private static final Logger LOG = LoggerFactory.getLogger(NewsService.class);

    private final NewsRepository newsRepository;
    private final INewsContentItemService iNewsContentItemService;

    private final CommentService newsCommentService;
    private final LikeService newsLikeService;
    private final UserService userservice;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public News createNews(News body, String user) {
        NewsContentItem newsContentItem = new NewsContentItem();
        newsContentItem.setTitle(body.getTitle());
        newsContentItem.setShortDescription(body.getShortDescription());
        newsContentItem.setContent(body.getContent());
        NewsContentItem savedItem = iNewsContentItemService.createNewsContent(newsContentItem);
        body.setContent(null);
        body.setShortDescription(null);
        body.setTitle(null);
        body.setNewsItemId(savedItem.getId().toString());
        body.setCreatedBy(user);
        body.setStatus(false);
        News newsEntity = newsRepository.save(body);

        LOG.debug("createNews: entity created for newsId: {}", body.getId());
        return newsEntity;
    }

    @Override
    public News getNews(Long newsId, String user) {
        News response = newsRepository.findByIdAndStatusIsTrue(newsId).orElseThrow();
        NewsContentItem newsContentItem = iNewsContentItemService.getNewsContentById(response.getNewsItemId());

        response.setTitle(newsContentItem.getTitle());
        response.setShortDescription(newsContentItem.getShortDescription());
        response.setContent(newsContentItem.getContent());

        // Get and set tags
        response.setLikeCount(newsLikeService.getLikeCountByNewsId(newsId));
        response.setCommentCount(newsCommentService.getCommentCountByNewsId(newsId));
        if (user != null && !user.isEmpty()){
            response.setIsLiked(newsLikeService.isLiked(newsId, user));
        }
        LOG.debug("getNews: found newsId: {}", response.getId());

        return response;

    }

    @Override
    public PageableCustom getNewsAll(Map<String, String> params, String user) {
        int pageNumber = 0;
        int pageSize = 10;

        if (params.containsKey("pageNumber")) {
            pageNumber = Integer.parseInt(params.get("pageNumber"));
        }
        if (params.containsKey("pageSize")) {
            pageSize = Integer.parseInt(params.get("pageSize"));
        }

        final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> cq = cb.createQuery(News.class);
        Root<News> iRoot = cq.from(News.class);
        List<Predicate> predicates = new ArrayList<>();
        //status

        if (params.containsKey("searchString")) {
            predicates.add(cb.or(cb.like(cb.lower(iRoot.get("title")), "%" + params.get("searchString").toLowerCase() + "%")));
        }

        if (params.containsKey("authorId")) {
            predicates.add(cb.equal(iRoot.get("authorId"), params.get("authorId")));
        }

        if (params.containsKey("tabId")) {
            predicates.add(cb.equal(iRoot.get("tabId"), params.get("tabId")));
        }
        predicates.add(cb.equal(iRoot.get("status"), true));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);

        cq.where(predArray);

        TypedQuery<News> query = entityManager.createQuery(cq);
        int totalRows = query.getResultList().size();

        query.setFirstResult(pageableRequest.getPageNumber() * pageableRequest.getPageSize());
        query.setMaxResults(pageableRequest.getPageSize());

        Page<News> result = new PageImpl<>(query.getResultList(), pageableRequest, totalRows);

        List<News> newsList = query.getResultList().stream()
                .map(news -> {
                    News entity = new News();
                    NewsContentItem newsContentItem = iNewsContentItemService.getNewsContentById(news.getNewsItemId());

                    entity.setTitle(newsContentItem.getTitle());
                    entity.setShortDescription(newsContentItem.getShortDescription());
                    entity.setContent(newsContentItem.getContent());

                    entity.setId(news.getId());
                    entity.setContent(news.getContent());
                    entity.setPublishedDate(news.getPublishedDate());
                    entity.setAuthorId(news.getAuthorId());
                    entity.setStatus(news.getStatus());
                    entity.setTabId(news.getTabId());
                    entity.setLikeCount(newsLikeService.getLikeCountByNewsId(news.getId()));
                    entity.setCommentCount(newsCommentService.getCommentCountByNewsId(news.getId()));
                    if (user != null && !user.isEmpty()) {
                        entity.setIsLiked(newsLikeService.isLiked(news.getId(), user));
                    }
                    return entity;
                })
                .toList();
        ;

        PageableCustom pageableCustom = new PageableCustom();

        pageableCustom.setContent(newsList);
        pageableCustom.setPage(result.getNumber());
        pageableCustom.setSize(result.getSize());
        pageableCustom.setTotal(result.getTotalElements());

        return pageableCustom;
    }

    @Override
    public PageableCustom getNewsPageablePortal(Map<String, String> params, String user) {
        int pageNumber = 0;
        int pageSize = 10;

        if (params.containsKey("pageNumber")) {
            pageNumber = Integer.parseInt(params.get("pageNumber"));
        }
        if (params.containsKey("pageSize")) {
            pageSize = Integer.parseInt(params.get("pageSize"));
        }

        final Pageable pageableRequest = PageRequest.of(pageNumber, pageSize);

        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<News> cq = cb.createQuery(News.class);
        Root<News> iRoot = cq.from(News.class);
        List<Predicate> predicates = new ArrayList<>();
        //status

        if (params.containsKey("searchString")) {
            predicates.add(cb.or(cb.like(cb.lower(iRoot.get("title")), "%" + params.get("searchString").toLowerCase() + "%")));
        }

        if (params.containsKey("authorId")) {
            predicates.add(cb.equal(iRoot.get("authorId"), params.get("authorId")));
        }

        if (params.containsKey("tabId")) {
            predicates.add(cb.equal(iRoot.get("tabId"), params.get("tabId")));
        }
        predicates.add(cb.equal(iRoot.get("status"), false));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);

        cq.where(predArray);

        TypedQuery<News> query = entityManager.createQuery(cq);
        int totalRows = query.getResultList().size();

        query.setFirstResult(pageableRequest.getPageNumber() * pageableRequest.getPageSize());
        query.setMaxResults(pageableRequest.getPageSize());

        Page<News> result = new PageImpl<>(query.getResultList(), pageableRequest, totalRows);

        List<News> newsList = query.getResultList().stream()
                .map(news -> {
                    News entity = new News();
                    NewsContentItem newsContentItem = iNewsContentItemService.getNewsContentById(news.getNewsItemId());

                    entity.setTitle(newsContentItem.getTitle());
                    entity.setShortDescription(newsContentItem.getShortDescription());
                    entity.setContent(newsContentItem.getContent());

                    entity.setId(news.getId());
                    entity.setContent(news.getContent());
                    entity.setPublishedDate(news.getPublishedDate());
                    entity.setAuthorId(news.getAuthorId());
                    entity.setStatus(news.getStatus());
                    entity.setTabId(news.getTabId());
                    entity.setLikeCount(newsLikeService.getLikeCountByNewsId(news.getId()));
                    entity.setCommentCount(newsCommentService.getCommentCountByNewsId(news.getId()));
                    if (user != null && !user.isEmpty()) {
                        entity.setIsLiked(newsLikeService.isLiked(news.getId(), user));
                    }
                    return entity;
                })
                .toList();
        ;

        PageableCustom pageableCustom = new PageableCustom();

        pageableCustom.setContent(newsList);
        pageableCustom.setPage(result.getNumber());
        pageableCustom.setSize(result.getSize());
        pageableCustom.setTotal(result.getTotalElements());

        return pageableCustom;
    }

    @Override
    public List<News> getTOPLikeNews(String username){
        return newsRepository.findTOPNewsByLike().stream()
                .map(news -> {
                    News entity = new News();
                    NewsContentItem newsContentItem = iNewsContentItemService.getNewsContentById(news.getNewsItemId());

                    entity.setTitle(newsContentItem.getTitle());
                    entity.setShortDescription(newsContentItem.getShortDescription());
                    entity.setContent(newsContentItem.getContent());

                    entity.setId(news.getId());
                    entity.setTitle(news.getTitle());
                    entity.setDescription(news.getDescription());
                    entity.setShortDescription(news.getShortDescription());
                    entity.setContent(news.getContent());
                    entity.setPublishedDate(news.getPublishedDate());
                    entity.setAuthorId(news.getAuthorId());
                    entity.setStatus(news.getStatus());
                    entity.setTabId(news.getTabId());
                    entity.setLikeCount(newsLikeService.getLikeCountByNewsId(news.getId()));
                    entity.setCommentCount(newsCommentService.getCommentCountByNewsId(news.getId()));
                    if (username != null && !username.isEmpty()) {
                        entity.setIsLiked(newsLikeService.isLiked(news.getId(), username));
                    }
                    return entity;
                })
                .collect(Collectors.toList());
    }

    @Override
    public List<News> getTOPCommentNews(String username){
        return newsRepository.findTOPNewsByComment();
    }

    @Override
    public List<News> getNewsByStatus(Boolean status, String username) {
        List<News> entityList = newsRepository.findAllByStatus(status);
        List<User> userList = userservice.getUsersById(entityList.stream()
                .map(News::getAuthorId)
                .collect(Collectors.toList()));
        return entityList
                .stream()
                .map(news -> {
                    News entity = new News();
                    NewsContentItem newsContentItem = iNewsContentItemService.getNewsContentById(news.getNewsItemId());

                    entity.setTitle(newsContentItem.getTitle());
                    entity.setShortDescription(newsContentItem.getShortDescription());
                    entity.setContent(newsContentItem.getContent());

                    entity.setId(news.getId());
                    entity.setTitle(news.getTitle());
                    entity.setDescription(news.getDescription());
                    entity.setShortDescription(news.getShortDescription());
                    entity.setContent(news.getContent());
                    entity.setPublishedDate(news.getPublishedDate());
                    entity.setAuthorId(news.getAuthorId());
                    entity.setStatus(news.getStatus());
                    entity.setAuthorId(news.getAuthorId());
                    entity.setTabId(news.getTabId());
                    entity.setLikeCount(newsLikeService.getLikeCountByNewsId(news.getId()));
                    entity.setCommentCount(newsCommentService.getCommentCountByNewsId(news.getId()));
                    if (username != null && !username.isEmpty()) {
                        entity.setIsLiked(newsLikeService.isLiked(news.getId(), username));
                    }
                    return entity;
                })
                .collect(Collectors.toList());

    }

    @Override
    public List<News> getUsersNewsByStatus(Boolean status, String username){
        Long userId = userservice.getUserIdByUsername(username);
        List<News> newsEntityList = newsRepository.findAllByStatusAndAuthorId(status, userId);
        List<User> userList = userservice.getUsersById(newsEntityList.stream()
                .map(News::getAuthorId)
                .collect(Collectors.toList()));

        return newsEntityList.stream()
                .map(news -> {
                    News entity = new News();

                    NewsContentItem newsContentItem = iNewsContentItemService.getNewsContentById(news.getNewsItemId());

                    entity.setTitle(newsContentItem.getTitle());
                    entity.setShortDescription(newsContentItem.getShortDescription());
                    entity.setContent(newsContentItem.getContent());

                    entity.setId(news.getId());
                    entity.setTitle(news.getTitle());
                    entity.setDescription(news.getDescription());
                    entity.setShortDescription(news.getShortDescription());
                    entity.setContent(news.getContent());
                    entity.setPublishedDate(news.getPublishedDate());
                    entity.setAuthorId(news.getAuthorId());
                    entity.setStatus(news.getStatus());
                    entity.setAuthorId(news.getAuthorId());
                    entity.setTabId(news.getTabId());
                    entity.setLikeCount(newsLikeService.getLikeCountByNewsId(news.getId()));
                    entity.setCommentCount(newsCommentService.getCommentCountByNewsId(news.getId()));
                    if (username != null && !username.isEmpty()) {
                        entity.setIsLiked(newsLikeService.isLiked(news.getId(), username));
                    }
                    return entity;
                })
                .collect(Collectors.toList());

    }

    @Override
    public News updateNews(Long newsId, News body, String user) {
        News entity = newsRepository.findById(newsId)
                .orElseThrow();

        NewsContentItem newsContentItem = iNewsContentItemService.getNewsContentById(entity.getNewsItemId());
        newsContentItem.setTitle(body.getTitle());
        newsContentItem.setShortDescription(body.getShortDescription());
        newsContentItem.setContent(body.getContent());
        iNewsContentItemService.createNewsContent(newsContentItem);

        entity.setContent(body.getContent());
        entity.setPublishedDate(body.getPublishedDate());
        entity.setAuthorId(body.getAuthorId());
        entity.setTabId(body.getTabId());
        entity.setLastModifiedBy(user);
        entity.setLastModifiedDate(LocalDateTime.now());
        entity.setStatus(body.getStatus());
        News newEntity = newsRepository.save(entity);

        LOG.debug("updateNews: updated an entity with newsId: {}", newsId);
        return newEntity;

    }

    @Override
    public News updateNewsStatus(Long newsId, Boolean status, String user) {
        News entity = newsRepository.findById(newsId)
                .orElseThrow();

        entity.setStatus(status);
        entity.setLastModifiedBy(user);
        entity.setLastModifiedDate(LocalDateTime.now());
        entity.setPublishedDate(LocalDateTime.now());

        News newsEntity = newsRepository.save(entity);

        LOG.debug("updateNewsStatus: updated an entity with newsId: {}", newsId);
        return newsEntity;

    }

    @Override
    public void deleteNews(Long newsId, String user) {
        LOG.debug("deleteNews: tries to delete an entity with newsId: {}", newsId);
        newsRepository.findById(newsId).ifPresent(o -> {
            newsCommentService.deleteAllNewsCommentByNewsId(newsId, user);
            newsLikeService.deleteAllByNewsId(newsId);

            newsRepository.deleteById(newsId);
        });

    }

    @Override
    @Transactional
    public void deleteNewsByAuthorId(Long authorId, String user){
        LOG.debug("deleteNews: tries to delete an entity with authorId: {}", authorId);
        Long userId = userservice.getUserIdByUsername(user);

        newsRepository.deleteAllByAuthorId(authorId);

    }

    @Transactional
    @Override
    public void deleteAllNewsByCategoryId(Long tabId, String user) {
        LOG.debug("deleteAllNewsByCategoryId: tries to delete all entities with tabId: {}", tabId);
        //checking whether tab with such tabId exist ??

        newsRepository.deleteAllByTabId(tabId);
    }

    @Override
    public List<News> getAllNewsByAuthorId(Long authorId, String username) {
        List<News> entityList = newsRepository.findAllByAuthorId(authorId);
        List<User> userList = userservice.getUsersById(entityList.stream()
                .map(News::getAuthorId)
                .collect(Collectors.toList()));
        return entityList
                .stream()
                .map(news -> {
                    News entity = new News();

                    NewsContentItem newsContentItem = iNewsContentItemService.getNewsContentById(news.getNewsItemId());

                    entity.setTitle(newsContentItem.getTitle());
                    entity.setShortDescription(newsContentItem.getShortDescription());
                    entity.setContent(newsContentItem.getContent());

                    entity.setId(news.getId());
                    entity.setTitle(news.getTitle());
                    entity.setDescription(news.getDescription());
                    entity.setShortDescription(news.getShortDescription());
                    entity.setContent(news.getContent());
                    entity.setPublishedDate(news.getPublishedDate());
                    entity.setAuthorId(news.getAuthorId());
                    entity.setStatus(news.getStatus());
                    entity.setAuthorId(news.getAuthorId());
                    entity.setTabId(news.getTabId());
                    entity.setLikeCount(newsLikeService.getLikeCountByNewsId(news.getId()));
                    entity.setCommentCount(newsCommentService.getCommentCountByNewsId(news.getId()));
                    if (username != null && !username.isEmpty()) {
                        entity.setIsLiked(newsLikeService.isLiked(news.getId(), username));
                    }
                    return entity;
                })
                .collect(Collectors.toList());

    }

    @KafkaListener(topics = "mine", groupId = "group_json",
            containerFactory = "userKafkaListenerFactory")
    public void consumeJson(Category category) {
        log.info("Consumed JSON Message: " + category);
        deleteAllNewsByCategoryId(category.getId(), category.getName());
    }
}
