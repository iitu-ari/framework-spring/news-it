package kz.news.it.service;

import kz.news.it.entity.News;
import kz.news.it.entity.PageableCustom;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public interface INewsService {
    News createNews(News body, String user);
    News getNews(Long newsId, String user);
    PageableCustom getNewsAll(Map<String, String> params, String user);
    PageableCustom getNewsPageablePortal(Map<String, String> params, String user);
    List<News> getTOPLikeNews(String username);
    List<News> getTOPCommentNews(String username);
    List<News> getNewsByStatus(Boolean status, String username);
    List<News> getUsersNewsByStatus(Boolean status, String username);
    News updateNews(Long newsId, News body, String user);
    News updateNewsStatus(Long newsId, Boolean status, String user);
    void deleteNews(Long newsId, String user);
    void deleteNewsByAuthorId(Long authorId, String user);
    void deleteAllNewsByCategoryId(Long tabId, String user);
    List<News> getAllNewsByAuthorId(Long authorId, String username);
}
