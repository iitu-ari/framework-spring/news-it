package kz.news.it.service;

import kz.news.it.entity.Comment;
import kz.news.it.entity.PageableCustom;

import java.util.List;
import java.util.Map;

public interface ICommentService {
    Comment createNewsComment(Comment body, String user);
    Comment getNewsComment(Long newsCommentId);
    List<Comment> getAllNewsCommentByNewsId(Long newsId);
    List<Comment> getAllNewsCommentByUserId(Long newsId);
    List<Comment> getAllNewsCommentByUserIdAndNewsId(Long authorId, Long newsId);
    PageableCustom getAllNewsCommentByNewsIdPagination(Map<String, String> params);
    Comment updateNewsComment(Long newsCommentId, Comment body, String user);
    void deleteNewsComment(Long newsCommentId, String user);
    void deleteAllNewsCommentByNewsId(Long newsId, String user);
    void deleteAllNewsCommentByTabId(Long tabId, String user);
    void deleteAllNewsCommentByAuthorId(Long authorId);
    void deleteAllNewsCommentByAuthorIdAndNewsId(Long authorId, Long newsId);
    Long getCommentCountByNewsId(Long newsId);
}
