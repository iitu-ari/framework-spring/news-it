package kz.news.it.repository;

import kz.news.it.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {
    Optional<Comment> findById(Long id);
    List<Comment> findAllByNewsId(Long newsId);
    List<Comment> findAllByUserId(Long userId);
    List<Comment> findAllByUserIdAndNewsId(Long userId, Long newsId);
    Long countAllByNewsId(Long newsId);

    @Query(value = "select nc.* from _news_comment nc " +
            "join _news n on nc.news_id = n.id " +
            "where n.tab_id = :tabId", nativeQuery = true)
    List<Comment> findAllByTabId(Long tabId);

    @Modifying
    @Query(value = "delete from _news_comment where id in (select nc.id from _news_comment nc " +
            "join _news n on nc.news_id = n.id " +
            "where n.tab_id = :tabId)", nativeQuery = true)
    void deleteAllByTabId(Long tabId);

    void deleteAllByUserId(Long id);
    void deleteAllByUserIdAndNewsId(Long userId, Long newsId);
}
