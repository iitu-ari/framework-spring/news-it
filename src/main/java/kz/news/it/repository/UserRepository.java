package kz.news.it.repository;

import kz.news.it.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findById(Long id);
    Page<User> findAll(Pageable pageable);
    List<User> findByIdIn(List<Long> idList);
    List<User> findAllByUsername(String username);
    List<User> findAllByEmail(String email);
    void deleteById(Long id);
    User getById(Long id);
    User findByUsername(String username);
}
