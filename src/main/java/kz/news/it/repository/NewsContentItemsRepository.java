package kz.news.it.repository;

import kz.news.it.model.NewsContentItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
    public interface NewsContentItemsRepository extends MongoRepository<NewsContentItem, String> {

    @Query("{'$or': [" +
            "{'content': {$regex: ?0, $options: 'i'}}, " +
            "{'title': {$regex: ?0, $options: 'i'}}, " +
            "{'shortDescription': {$regex: ?0, $options: 'i'}} ]}")
    List<NewsContentItem> globalSearch(final String value);

    @Query("{'news_id': ?0}")
    NewsContentItem findByNewsId(Long newsId);
}
