package kz.news.it.repository;

import kz.news.it.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface NewsRepository extends JpaRepository<News, Long> {
    Optional<News> findByIdAndStatusIsTrue(Long id);
    Optional<News> findByIdAndAuthorId(Long newsId, Long authorId);
    Optional<News> findByAuthorId(Long authorId);
    List<News> findAllByTabId(Long tabId);
    List<News> findAllByAuthorId(Long authorId);
    List<News> findAll();
    List<News> findAllByStatus(Boolean status);
    List<News> findAllByStatusAndAuthorId(Boolean status, Long authorId);
    void deleteAllByAuthorId(Long authorId);

    @Query(value = "select n.*, (select count(nl) from _news_like nl where nl.news_id = n.id and nl.status is true) as likedCount from _news n " +
            "where n.created_date > CURRENT_DATE - INTERVAL '1 months' " +
            "group by n.id " +
            "order by likedCount desc " +
            "limit 10", nativeQuery = true)
    List<News> findTOPNewsByLike();

    @Query(value = "select n.*, (select count(nc) from _news_comment nc where nc.news_id = n.id) as commentCount from _news n " +
            "where n.created_date > CURRENT_DATE - INTERVAL '1 months' " +
            "group by n.id " +
            "order by commentCount desc " +
            "limit 10", nativeQuery = true)
    List<News> findTOPNewsByComment();

    @Modifying
    void deleteAllByTabId(Long tabId);

}
