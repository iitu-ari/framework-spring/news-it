package kz.news.it.repository;

import kz.news.it.entity.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public interface LikeRepository extends JpaRepository<Like, Long> {
    void deleteAllByNewsId(Long newsId);
    void deleteById(Long id);
    Like findByUserIdAndNewsId(Long userId, Long newsId);

    @Query(value = "select count(*) from _news_like nl " +
            "where nl.news_id = :newsId and nl.status is true", nativeQuery = true)
    Long countAllByNewsId(Long newsId);

    @Query(value = "select nc.* from _news_like nc " +
            "join _news n on nc.news_id = n.id " +
            "where n.tab_id = :tabId", nativeQuery = true)
    List<Like> findAllByTabId(Long tabId);

    @Modifying
    @Query(value = "delete from _news_like where id in (select nc.id from _news_like nc " +
            "join _news n on nc.news_id = n.id " +
            "where n.tab_id = :tabId)", nativeQuery = true)
    void deleteAllByTabId(Long tabId);

}
