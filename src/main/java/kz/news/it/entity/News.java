package kz.news.it.entity;


import lombok.*;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "_news", schema = "public")
@Data
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Builder
@AllArgsConstructor
public class News extends AbstractAuditingEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "short_description")
    private String shortDescription;

    @Column(name = "content")
    private String content;

    @Column(name = "published_date")
    private LocalDateTime publishedDate;

    @Column(name = "author_id")
    private Long authorId;

    @Column(name = "tab_id")
    private Long tabId;

    //blocked t f
    @Column(name = "status")
    private Boolean status = false;

    @Column(name = "news_item_id")
    private String newsItemId;

    @Transient
    private Long commentCount;
    @Transient
    private Long likeCount;
    @Transient
    private Boolean isLiked;

    public News(){
        this.id = 0L;
        this.title = null;
        this.description = null;
        this.shortDescription = null;
        this.content = null;
        this.publishedDate = null;
        this.authorId = 0L;
        this.tabId = 0L;
        this.status = null;
        this.commentCount = 0L;
        this.likeCount = 0L;
        this.isLiked = false;
    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(String description) {
//        this.description = description;
//    }
//
//    public String getShortDescription() {
//        return shortDescription;
//    }
//
//    public void setShortDescription(String shortDescription) {
//        this.shortDescription = shortDescription;
//    }
//
//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }
//
//    public LocalDateTime getPublishedDate() {
//        return publishedDate;
//    }
//
//    public void setPublishedDate(LocalDateTime publishedDate) {
//        this.publishedDate = publishedDate;
//    }
//
//    public Long getAuthorId() {
//        return authorId;
//    }
//
//    public void setAuthorId(Long authorId) {
//        this.authorId = authorId;
//    }
//
//    public Long getTabId() {
//        return tabId;
//    }
//
//    public void setTabId(Long tabId) {
//        this.tabId = tabId;
//    }
//
//    public Boolean getStatus() {
//        return status;
//    }
//
//    public void setStatus(Boolean status) {
//        this.status = status;
//    }
//
//    public Long getCommentCount() {
//        return commentCount;
//    }
//
//    public void setCommentCount(Long commentCount) {
//        this.commentCount = commentCount;
//    }
//
//    public Long getLikeCount() {
//        return likeCount;
//    }
//
//    public void setLikeCount(Long likeCount) {
//        this.likeCount = likeCount;
//    }
//
//    public Boolean getIsLiked() {
//        return isLiked;
//    }
//
//    public void setIsLiked(Boolean liked) {
//        isLiked = liked;
//    }

}
