package kz.news.it.entity;

import lombok.Data;

@Data
public class UserDTO {
    String username;
    String email;
    String password;
    Boolean isAdmin;
}
