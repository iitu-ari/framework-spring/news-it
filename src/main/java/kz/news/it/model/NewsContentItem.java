package kz.news.it.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "newsContentItems")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NewsContentItem {

    @Id
    private ObjectId id;

    private String content;

    private String title;

    private String shortDescription;

    private Long news_id;
}
