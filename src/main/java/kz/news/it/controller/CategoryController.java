package kz.news.it.controller;

import kz.news.it.entity.Category;
import kz.news.it.entity.PageableCustom;
import kz.news.it.service.ICategoryService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/category")
@AllArgsConstructor
@Slf4j
public class CategoryController {
    private ICategoryService categoryService;

    @PostMapping
    public ResponseEntity<Category> saveUser(@RequestBody Category category, @RequestHeader(value = "username", required = false) String user) {
        return ResponseEntity.ok(categoryService.createCategory(category, user));
    }

    @GetMapping
    public List<Category> getAll(){
        return categoryService.getAllCategories();
    }

    @GetMapping("/{id}")
    public Category getCategory(@PathVariable Long id){
        return categoryService.getCategory(id);
    }

    @PutMapping("{id}")
    public Category updateCategory(@PathVariable Long id, @RequestBody Category body, @RequestHeader(value = "username", required = false) String user){
        return categoryService.updateCategory(id, body, user);
    }

    @DeleteMapping("{id}")
    public void deleteCategory(@PathVariable Long id, @RequestHeader(value = "username", required = false) String user) {
        categoryService.deleteCategory(id, user);
    }

    @GetMapping("/filtered")
    public PageableCustom getFilteredCategories(@RequestParam Map<String, String> params){
        return categoryService.getFilteredCategories(params);
    }
}
