package kz.news.it.controller;

import kz.news.it.entity.Like;
import kz.news.it.service.ILikeService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/like")
@AllArgsConstructor
@Slf4j
public class LikeController {

    private final ILikeService likeService;

    @PostMapping
    public Like create(@RequestBody Like body){
        return likeService.createNewsLike(body);
    }

    @DeleteMapping("{newsId}")
    public void deleteAllByNewsId(@PathVariable Long newsId){
        likeService.deleteAllByNewsId(newsId);
    }

    @DeleteMapping("{categoryId}")
    public void deleteAllNewsLikeByTabId(@PathVariable Long categoryId){
        likeService.deleteAllNewsLikeByTabId(categoryId);
    }

    @GetMapping("{newsId}")
    public Long getLikeCountByNewsId(@PathVariable Long newsId){
     return likeService.getLikeCountByNewsId(newsId);
    }

    @GetMapping("/liked/{newsId}")
    public Boolean isLiked(@PathVariable Long newsId, @RequestHeader(value = "username", required = false) String user){
        return likeService.isLiked(newsId, user);
    }
}
