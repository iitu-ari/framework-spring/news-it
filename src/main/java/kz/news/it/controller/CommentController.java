package kz.news.it.controller;

import kz.news.it.entity.Comment;
import kz.news.it.entity.PageableCustom;
import kz.news.it.service.ICommentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/comment")
@Slf4j
@AllArgsConstructor
public class CommentController {
    private final ICommentService commentService;

    @PostMapping
    public Comment createNewsComment(@RequestBody Comment body, @RequestHeader(value = "username", required = false) String user){
        return commentService.createNewsComment(body, user);
    }

    @GetMapping("{id}")
    public Comment getNewsComment(@PathVariable Long id){
        return commentService.getNewsComment(id);
    }

    @GetMapping("/news/{newsId}")
    public List<Comment> getAllNewsComment(@PathVariable Long newsId){
        return commentService.getAllNewsCommentByNewsId(newsId);
    }

    @GetMapping("/author/{authorId}")
    public List<Comment> getAllNewsCommentByAuthor(@PathVariable Long authorId){
        return commentService.getAllNewsCommentByUserId(authorId);
    }

    @GetMapping("/author-news/{authorId}")
    public List<Comment> getAllNewsCommentByUserIdAndNewsId(@PathVariable Long authorId, @RequestParam Long newsId){
        return commentService.getAllNewsCommentByUserIdAndNewsId(authorId, newsId);
    }

    @GetMapping
    public PageableCustom getAllNewsCommentByNewsIdPagination(@RequestParam Map<String, String> params){
        return commentService.getAllNewsCommentByNewsIdPagination(params);
    }

    @PutMapping("{id}")
    public Comment updateNewsComment(@PathVariable Long id, @RequestBody Comment body, @RequestHeader(value = "username", required = false) String user){
        return commentService.updateNewsComment(id, body, user);
    }

    @DeleteMapping("{newsCommentId}")
    public void deleteNewsComment(@PathVariable Long newsCommentId, @RequestHeader(value = "username", required = false) String user){
        commentService.deleteNewsComment(newsCommentId, user);
    }

    @DeleteMapping("/news/{newsId}")
    public void deleteAllNewsCommentByNewsId(@PathVariable Long newsId, @RequestHeader(value = "username", required = false) String user){
        commentService.deleteAllNewsCommentByNewsId(newsId, user);
    }

    @DeleteMapping("/category/{categoryId}")
    public void deleteAllNewsCommentByTabId(@PathVariable Long categoryId, @RequestHeader(value = "username", required = false) String user){
        commentService.deleteAllNewsCommentByTabId(categoryId, user);
    }

    @DeleteMapping("/author/{authorId}")
    public void deleteAllNewsCommentByAuthorId(@PathVariable Long authorId, @RequestHeader(value = "username", required = false) String user){
        commentService.deleteAllNewsCommentByAuthorId(authorId);
    }

    @DeleteMapping("/author-news/{authorId}")
    public void deleteAllNewsCommentByAuthorIdAndNewsId(@PathVariable Long authorId, @RequestParam Long newsId, @RequestHeader(value = "username", required = false) String user){
        commentService.deleteAllNewsCommentByAuthorIdAndNewsId(authorId, newsId);
    }

    @GetMapping("/count/{newsId}")
    public Long getCommentCountByNewsId(@PathVariable Long newsId){
        return commentService.getCommentCountByNewsId(newsId);
    }
}
