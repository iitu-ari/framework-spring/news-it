package kz.news.it.controller;

import kz.news.it.entity.PageableCustom;
import kz.news.it.entity.User;
import kz.news.it.service.IUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
@Slf4j
public class UserController {

    private final IUserService userService;

    @PostMapping
    public ResponseEntity<User> saveUser(@RequestBody User userEntity) {
        return ResponseEntity.ok(userService.saveUser(userEntity));
    }

    @GetMapping
    public ResponseEntity<Page<User>> getAllUser(@RequestParam(defaultValue = "0") Integer pageNo,
                                                       @RequestParam(defaultValue = "10") Integer pageSize,
                                                       @RequestParam(defaultValue = "id") String sortBy) {
        return ResponseEntity.ok(userService.getAllUsers(pageNo, pageSize, sortBy));
    }

    @GetMapping("/{id}")
    public User getUserById(@PathVariable Long id) {
        return userService.getUserById(id);
    }

    @GetMapping("/user-list")
    public List<User> getUsersById(@RequestParam(value = "id-list") List<Long> idList) {
        return userService.getUsersById(idList);
    }


    @GetMapping(
            value = "/pageable",
            produces = "application/json")
    PageableCustom getFilteredCategories(@RequestParam Map<String, String> params) {

        return userService.getAllUserWithParams(params);
    }

    @PostMapping("/block-user/{id}")
    public boolean blockUsers(@PathVariable Long id) {
        return userService.blockUser(id);
    }

    @GetMapping("/unlock-user/{id}")
    public User unlockUserById(@PathVariable Long id) {
        return userService.unlockUserById(id);
    }

    @DeleteMapping("/{id}")
    public void  deleteUserById(@PathVariable Long id) {
        userService.deleteUserById(id);
    }

    @GetMapping("/user-id")
    public Long getUserIdByUsername(@RequestHeader(value = "username", required = false) String username) {
        return userService.getUserIdByUsername(username);
    }
}
