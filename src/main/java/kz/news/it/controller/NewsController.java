package kz.news.it.controller;

import kz.news.it.entity.News;
import kz.news.it.entity.PageableCustom;
import kz.news.it.service.INewsService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/news")
@AllArgsConstructor
@Slf4j
public class NewsController {
    private final INewsService newsService;

    @PostMapping
    public News createNews(@RequestBody News body, @RequestHeader(value = "username", required = false) String user){
        return newsService.createNews(body, user);
    }

    @GetMapping("{id}")
    public News getNews(@PathVariable Long id, @RequestHeader(value = "username", required = false) String user){
        return newsService.getNews(id, user);
    }

    @GetMapping
    public PageableCustom getAllNews(@RequestParam Map<String, String> params, @RequestHeader(value = "username", required = false) String user){
        return newsService.getNewsAll(params, user);
    }

    @GetMapping("/pageable")
    public PageableCustom getAllNewsPageable(@RequestParam Map<String, String> params, @RequestHeader(value = "username", required = false) String user){
        return newsService.getNewsPageablePortal(params, user);
    }

    @GetMapping("/top-like")
    public List<News> getTopLikeNews(@RequestHeader(value = "username", required = false) String user){
        return newsService.getTOPLikeNews(user);
    }

    @GetMapping("/top-comment")
    public List<News> getTopCommentNews(@RequestHeader(value = "username", required = false) String user){
        return newsService.getTOPCommentNews(user);
    }

    @GetMapping("/status")
    public List<News> getNewsByStatus(@RequestParam Boolean status, @RequestHeader(value = "username", required = false) String username){
        return newsService.getNewsByStatus(status, username);
    }

    @GetMapping("/user")
    public List<News> getUserNewsByStatus(@RequestParam Boolean status, @RequestHeader(value = "username", required = false) String username){
        return newsService.getUsersNewsByStatus(status, username);
    }

    @PutMapping("{id}")
    public News updateNews(@PathVariable Long id, @RequestBody News body, @RequestHeader(value = "username", required = false) String user){
        return newsService.updateNews(id, body, user);
    }

    @PutMapping("/status/{id}")
    public News updateNewsStatus(@PathVariable Long id, @RequestParam Boolean status, @RequestHeader(value = "username", required = false) String user){
        return newsService.updateNewsStatus(id, status, user);
    }

    @DeleteMapping("{id}")
    public void deleteNews(@PathVariable Long id, @RequestHeader(value = "username", required = false) String user){
        newsService.deleteNews(id, user);
    }

    @DeleteMapping("/author/{id}")
    public void deleteAuthorNews(@PathVariable Long id, @RequestHeader(value = "username", required = false) String user){
        newsService.deleteNewsByAuthorId(id, user);
    }

    @DeleteMapping("/category/{tabId}")
    public void deleteCategoryNews(@PathVariable Long tabId, @RequestHeader(value = "username", required = false) String user){
        newsService.deleteAllNewsByCategoryId(tabId, user);
    }

    @GetMapping("/author/{authorId}")
    public List<News> getAllNewsByAuthorId(@PathVariable Long authorId, @RequestHeader(value = "username", required = false) String user){
        return newsService.getAllNewsByAuthorId(authorId, user);
    }
}
